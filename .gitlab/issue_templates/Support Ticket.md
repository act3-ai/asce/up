# ASCEup Support Ticket

I am requesting support for an ASCEup tool.

/label Support Ticket
/cc @justenstall

Consult the documentation:

- **[ASCEup Documentation](https://gitlab.com/act3-ai/asce/up)**
- **[ASCEup Troubleshooting](https://gitlab.com/act3-ai/asce/up/-/blob/main/docs/troubleshooting-faq.md)**
- **[ACT3 Login Documentation](https://gitlab.com/act3-ai/asce/up/-/tree/main/act3-login)**

## Tool

Select the tool you are requesting support for:

- [ ] ACT3 Login
- [ ] DoD Certificates script
- [ ] Setup Smart Card Reader script
- [ ] ASCE Tools

## Summary

Concisely summarize your issue:

## Steps to reproduce

Describe how you encountered the issue:

## Attempted Troubleshooting

Describe the troubleshooting you have attempted:

## System Information

Select the operating system you are running the tool on:

- [ ] Ubuntu 22.04 (full installation)
- [ ] macOS
- [ ] Windows running Ubuntu 22.04 on WSL2
- [ ] Other:

## Log File

> ACT3 Login creates a log file to store information about its execution that is important for diagnosing your problem. The path to the log file is printed in your terminal with the message "Created log file **FILE**".
>
> - For Linux/WSL systems, the default location is `~/.cache/up/act3-login/logs.txt`
> - On macOS, the default location is `~/Library/Caches/up/act3-login/logs.txt`

Attach the log file created by ACT3 Login.

## Terminal output

Provide the tool's terminal output in the code block below:

```plaintext

```

## Other logs and/or screenshots

Include any other relevant logs below using code blocks:

## Possible fixes

If you can, describe what you think may have caused the problem:
